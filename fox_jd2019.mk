#
#	This file is part of the OrangeFox Recovery Project
# 	Copyright (C) 2023 The OrangeFox Recovery Project
#
#	OrangeFox is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	any later version.
#
#	OrangeFox is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
# 	This software is released under GPL version 3 or any later version.
#	See <http://www.gnu.org/licenses/>.
#
# 	Please maintain this if you use this script or any part of it
#

# OrangeFox-specific settings #

# OrangeFox
FOX_BUILD_DEVICE := jd2019
FOX_USE_NANO_EDITOR := true
FOX_USE_BASH_SHELL := true
FOX_USE_TWRP_RECOVERY_IMAGE_BUILDER := 1

OF_KEEP_FORCED_ENCRYPTION := 1
OF_SCREEN_H := 2340
OF_STATUS_H := 80
OF_STATUS_INDENT_LEFT := 48
OF_STATUS_INDENT_RIGHT := 48
OF_HIDE_NOTCH := 0
OF_CLOCK_POS := 1
OF_USE_MAGISKBOOT := 1
OF_USE_MAGISKBOOT_FOR_ALL_PATCHES := 1
OF_DONT_PATCH_ENCRYPTED_DEVICE := 1
OF_NO_TREBLE_COMPATIBILITY_CHECK := 1
OF_NO_MIUI_PATCH_WARNING := 1
OF_USE_GREEN_LED := 1

# remove addons
FOX_DELETE_AROMAFM := 1
FOX_DELETE_INITD_ADDON := 1
FOX_USE_TAR_BINARY := 1
FOX_USE_ZIP_BINARY := 1
FOX_USE_SED_BINARY := 1
FOX_USE_XZ_UTILS := 1
FOX_REPLACE_BUSYBOX_PS := 1

OF_USE_NEW_MAGISKBOOT := 1
OF_SKIP_MULTIUSER_FOLDERS_BACKUP := 1

# use system (ROM) fingerprint where available
OF_USE_SYSTEM_FINGERPRINT := 1
# OTA for custom ROMs
OF_SUPPORT_ALL_BLOCK_OTA_UPDATES := 1
OF_FIX_OTA_UPDATE_MANUAL_FLASH_ERROR := 1
OF_MAINTAINER := CakesTwix